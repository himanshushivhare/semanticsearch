﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using weka.classifiers;
using weka.classifiers.functions;
using weka.core;
using weka.filters;

namespace Test1
{
    class TrainNN
    {
        public static int NNnumattrs = 11;
        public static void train()
        {
            SqlConnection myConnection = new SqlConnection("user id=shivhare.1;" +
                                       "password=abc@123;server=localhost;" +
                                       "Trusted_Connection=yes;" +
                                       "database=test; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {

                Instances[] datasets = new Instances[21];
                for (int i = 0; i < 21; i++)
                {
                    datasets[i] = null;
                }

                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from PositionDescriptions", myConnection);
                myReader = myCommand.ExecuteReader();

                List<String> docs = new List<String>();
                FastVector attrs = new FastVector();
                weka.core.Attribute id = new weka.core.Attribute("id");
                attrs.addElement(id);
                weka.core.Attribute type = new weka.core.Attribute("type");
                attrs.addElement(type);
                weka.core.Attribute pref = new weka.core.Attribute("pref");
                attrs.addElement(pref);
                weka.core.Attribute dataattr = new weka.core.Attribute("data", (FastVector)null);
                attrs.addElement(dataattr);

                Instances resumes = new Instances("resumes", attrs, 10000);
                Instances queries = new Instances("queries", attrs, 10000);


                while (myReader.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(queries);
                    inst.setValue(id, int.Parse(myReader["Id"].ToString()));
                    inst.setValue(type, 0);
                    inst.setValue(pref, int.Parse(myReader["docCluster"].ToString()));
                    //if (!(myReader["ContractorEducation"] == DBNull.Value || myReader["ContractorSkills"] == DBNull.Value || myReader["ContractorWorkExperience"] == DBNull.Value))
                    if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader["Duties"].ToString() + myReader["WorkerCharacteristics"].ToString() + myReader["MinQualifications"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        queries.add(inst);
                    }
                }
                //Console.WriteLine(In.instance(1).stringValue(dataattr));
                myReader.Close();

                SqlCommand myCommand2 = new SqlCommand("select * from ContractorResumes", myConnection);
                SqlDataReader myReader2 = myCommand2.ExecuteReader();

                while (myReader2.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(resumes);
                    inst.setValue(id, int.Parse(myReader2["Id"].ToString()));
                    inst.setValue(type, 1);
                    if (!(myReader2["prefCluster"] == DBNull.Value))
                        inst.setValue(pref, int.Parse(myReader2["prefCluster"].ToString()));
                    else
                        inst.setValue(pref, 0);
                    if (!(myReader2["ContractorEducation"] == DBNull.Value || /*myReader["ContractorSkills"] == DBNull.Value ||*/ myReader2["ContractorWorkExperience"] == DBNull.Value))
                    //if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader2["ContractorEducation"].ToString() + myReader2["ContractorWorkExperience"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        resumes.add(inst);
                    }
                }
                myReader2.Close();


                weka.filters.unsupervised.attribute.StringToWordVector filter = new weka.filters.unsupervised.attribute.StringToWordVector();
                //filter.setUseStoplist(true);
                String[] options = new String[4];
                options[0] = "-I";
                options[1] = "-S";
                options[2] = "-L";
                options[3] = "-T";
                filter.setOptions(options);
                filter.setStemmer(new weka.core.stemmers.LovinsStemmer());
                weka.core.tokenizers.AlphabeticTokenizer tok = new weka.core.tokenizers.AlphabeticTokenizer();

                //tok.setNGramMaxSize(1);
                //tok.setNGramMinSize(1);
                filter.setTokenizer(tok);
                //filter.setOutputWordCounts(true);
                //filter.setIDFTransform(true);
                //filter.setInputFormat(In);
                //filter.setWordsToKeep(2000);
                filter.setDoNotOperateOnPerClassBasis(true);
                //Instances tfidf = Filter.useFilter(In, filter);

                FastVector attrs2 = new FastVector();
                weka.core.Attribute classdata = new weka.core.Attribute("class");
                attrs2.addElement(classdata);
                weka.core.Attribute resumedata = new weka.core.Attribute("resumedata", (FastVector) null);
                attrs2.addElement(resumedata);
                weka.core.Attribute resumedata2 = new weka.core.Attribute("resumedata2", (FastVector) null);
                attrs2.addElement(resumedata2);

                
                for (int i = 0; i < resumes.numInstances(); i++)
                {
                    if (resumes.instance(i).value(2) != 0)
                    {
                        int index = (int)resumes.instance(i).value(2);
                        Instances temp = null;
                        int numpos = 0;
                        int numneg = 0;
                        if (datasets[index] == null)
                        {
                            datasets[index] = new Instances("dataset" + index, attrs2, 1000000);
                            temp = datasets[index];
                        }
                        else
                            temp = datasets[index];

                        Instance resume = resumes.instance(i);

                        for (int j = 0; j < queries.numInstances(); j++)
                        {
                            
                            Instance query = queries.instance(j);
                            Instance inst = new Instance(attrs2.size());
                            inst.setDataset(temp);
                            inst.setValue(resumedata, resume.stringValue(3));
                            inst.setValue(resumedata2, query.stringValue(3));
                            if (query.value(2) == resume.value(2))
                            {
                                inst.setValue(classdata, 1);
                                if (numpos <= 1)
                                {
                                    temp.add(inst);
                                    numpos++;
                                }
                            }
                            else
                            {
                                inst.setValue(classdata, 0);
                                 if (numneg <= 1)
                                {
                                    temp.add(inst);
                                    numneg++;
                                }
                            }
                            //temp.add(inst);
                            if (numpos == 1 && numneg == 1)
                                break;
                            
                        }
                        temp.setClass(classdata);
                    }
                }

                for (int i = 0; i < 21; i++)
                {
                    if (datasets[i] != null)
                    {
                        Instances data = datasets[i];
                        filter.setInputFormat(data);
                        filter.setWordsToKeep(11);
                        Instances tfidf = Filter.useFilter(data, filter);
                        if (tfidf.numAttributes() != NNnumattrs)
                        {
                            while (tfidf.numAttributes() != NNnumattrs)
                                tfidf.deleteAttributeAt(tfidf.numAttributes() - 1);
                        }
                        MultilayerPerceptron NN = new MultilayerPerceptron();
                        NN.buildClassifier(tfidf);
                        MemoryStream mem = new MemoryStream();
                        StreamWriter sw = new StreamWriter(mem);
                        BinaryFormatter bf = new BinaryFormatter();
                        SqlCommand sqlCmd = new SqlCommand("INSERT INTO dbo.models VALUES (@id, @data, @ready, @clusterid)", myConnection);

                        sqlCmd.Parameters.Add("@id", SqlDbType.Int);
                        sqlCmd.Parameters.Add("@data", SqlDbType.VarBinary, Int32.MaxValue);
                        sqlCmd.Parameters.Add("@ready", SqlDbType.Int);
                        sqlCmd.Parameters.Add("@clusterid", SqlDbType.Int);

                        bf.Serialize(mem, NN);

                        sqlCmd.Parameters["@id"].Value = i;
                        sqlCmd.Parameters["@data"].Value = mem.GetBuffer();
                        sqlCmd.Parameters["@ready"].Value = 1;
                        sqlCmd.Parameters["@clusterid"].Value = i;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
        }
    }
}
