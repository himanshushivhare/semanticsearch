﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using weka.classifiers;
using weka.classifiers.functions;
using weka.core;
using weka.filters;

namespace Test1
{
    class ClusterQuery
    {
        public static int clusterQuery(String q)
        {
            int queryClusterID = -1; 
            SqlConnection myConnection = new SqlConnection("user id=shivhare.1;" +
                                       "password=abc@123;server=localhost;" +
                                       "Trusted_Connection=yes;" +
                                       "database=test; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from PositionDescriptions", myConnection);
                myReader = myCommand.ExecuteReader();

                List<String> docs = new List<String>();
                FastVector attrs = new FastVector();

                weka.core.Attribute dataattr = new weka.core.Attribute("data", (FastVector)null);
                attrs.addElement(dataattr);

                Instances In = new Instances("In", attrs, 10000);

                while (myReader.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(In);
                    //if (!(myReader["ContractorEducation"] == DBNull.Value || myReader["ContractorSkills"] == DBNull.Value || myReader["ContractorWorkExperience"] == DBNull.Value))
                    if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader["Duties"].ToString() + myReader["WorkerCharacteristics"].ToString() + myReader["MinQualifications"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        In.add(inst);
                    }
                }
                //Console.WriteLine(In.instance(1).stringValue(dataattr));
                myReader.Close();
                Instance temp = new Instance(attrs.size());
                temp.setDataset(In);
                //temp.setValue(dataattr, "( Bachelors, RPI, 7/1980, BSEE ) ( President, 9/2008, 1/1900, Consulting, 1-10, {{See www.kendoventures.com&#x0D;&#x0D;Kendo Ventures, LLC specializes in helping its clients improve their technology operations, develop new offerings, and start new businesses. With vast background in business strategy, IT, managed &amp; professional services, and technology solutions development, Kendo is uniquely qualified to help clients succeed.&#x0D;&#x0D;Started by Kenneth Kaisen in September, 2008, Kendo helps companies optimize their results by using time-tested processes and methodologies. Years of practical experience are applied to help transform, build, improve, or modify key elements of the company. &#x0D;&#x0D;Results are guaranteed and customer satisfaction is always the focus for all projects. Kendo operates as one of the team and gets deeply involved in the inner workings and success of its clients. The attitude and mindset of the firm is manifested in its name: Ken-do!}} ), ( Vice President &amp; General Manager, 6/2005, 9/2008, Technology, 1000+, {{Vice President / General Manager for Consulting and Systems Integration at Avaya, a $6B telecommunications and software company. In this capacity, Ken was accountable for the development and growth of Avaya’s worldwide professional services and systems integration business.  Ken’s team of 2,100 associates provided single point of contact for the planning, design, consulting, software, and support required for Avaya solutions.  Capabilities encompassed Avaya’s complete portfolio and partner software, including the deployment of VOIP, Contact Center and Unified Communications solutions.  Under Ken’s leadership, his business had the highest revenue and profit growth in Avaya and became a $470M business in 3 years with 25% annual growth.}} ), ( Vice President &amp; General Manager &amp; CIO, 1/1985, 6/2005, Technology, 1000+, {{Prior to Avaya, I was at the forefront of Xerox’s transition to a services/solutions-led company, with a track record of successful services, product, and IT leadership positions.  Most recently, I was VP/GM of the Enterprise Document Services Business Team for Xerox Global Services, where I had global responsibility for offering development, program management, marketing, field enablement and business results. With my team, I developed and brought to market the highly successful “Xerox Office Services” offering.  Previously I was VP/GM of the Business Innovation Practices at Xerox Global Services, where I creating and lead a new professional services team focused on delivering high-value consulting, services, and solutions to the company’s largest customers in North America .  I also served as the CIO for Xerox North America where I delivered several important new business applications and managed an IT budget of several hundred million dollars. Earlier in my Xerox career, I was the Chief Engineer for some of Xerox’s most successful software-intensive products and ran the corporate Strategy, Architecture, &amp; Compliance function.  I was also Corporate Vice President, CIO and led Marketing for New England Business Services, inc. in Boston, MA .}} )");
                temp.setValue(dataattr, q);
                In.add(temp);
                weka.filters.unsupervised.attribute.StringToWordVector filter = new weka.filters.unsupervised.attribute.StringToWordVector();
                //filter.setUseStoplist(true);
                String[] options = new String[4];
                options[0] = "-I";
                options[1] = "-S";
                options[2] = "-L";
                options[3] = "-T";
                filter.setOptions(options);
                filter.setStemmer(new weka.core.stemmers.LovinsStemmer());
                weka.core.tokenizers.AlphabeticTokenizer tok = new weka.core.tokenizers.AlphabeticTokenizer();

                //tok.setNGramMaxSize(1);
                //tok.setNGramMinSize(1);
                filter.setTokenizer(tok);
                //filter.setOutputWordCounts(true);
                //filter.setIDFTransform(true);
                filter.setInputFormat(In);
                //filter.setWordsToKeep(800);
                filter.setDoNotOperateOnPerClassBasis(true);
                Instances tfidf = Filter.useFilter(In, filter);
                Instances testdata = new Instances(tfidf, 10000);
                testdata.add(tfidf.instance(tfidf.numInstances() - 1));
                tfidf.delete(tfidf.numInstances() - 1);

                if (testdata.numAttributes() != GenerateQueryClusters.numattributes)
                {
                    while (testdata.numAttributes() != GenerateQueryClusters.numattributes)
                        testdata.deleteAttributeAt(testdata.numAttributes() - 1);
                }

                SqlCommand myCommand2 = new SqlCommand("select * from models", myConnection);
                myReader = myCommand2.ExecuteReader();

                while (myReader.Read())
                {
                    if (int.Parse(myReader["id"].ToString()) == GenerateQueryClusters.queryClustererID)
                    {
                        MemoryStream memStream = new MemoryStream((byte[])myReader["Data"]);
                        BinaryFormatter binForm = new BinaryFormatter();
                        memStream.Seek(0, SeekOrigin.Begin);
                        weka.clusterers.SimpleKMeans kmeans = (weka.clusterers.SimpleKMeans)binForm.Deserialize(memStream);
                        //if(kmeans.att
                        queryClusterID = kmeans.clusterInstance(testdata.lastInstance());
                    }
                }
                Console.WriteLine("cluster id for query string = " + queryClusterID);
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            return queryClusterID;
        }
    }
}
