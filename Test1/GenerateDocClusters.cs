﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using weka.classifiers;
using weka.classifiers.functions;
using weka.core;
using weka.filters;

namespace Test1
{
    class GenerateDocClusters
    {
        public static void genDocClusters()
        {
            SqlConnection myConnection = new SqlConnection("user id=shivhare.1;" +
                                       "password=abc@123;server=localhost;" +
                                       "Trusted_Connection=yes;" +
                                       "database=test; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from PositionDescriptions", myConnection);
                myReader = myCommand.ExecuteReader();

                List<String> docs = new List<String>();
                FastVector attrs = new FastVector();
                weka.core.Attribute id = new weka.core.Attribute("id");
                attrs.addElement(id);
                weka.core.Attribute type = new weka.core.Attribute("type");
                attrs.addElement(type);
                weka.core.Attribute dataattr = new weka.core.Attribute("data", (FastVector)null);
                attrs.addElement(dataattr);

                Instances In = new Instances("In", attrs, 10000);

                while (myReader.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(In);
                    inst.setValue(id, int.Parse(myReader["Id"].ToString()));
                    inst.setValue(type, 0);
                    //if (!(myReader["ContractorEducation"] == DBNull.Value || myReader["ContractorSkills"] == DBNull.Value || myReader["ContractorWorkExperience"] == DBNull.Value))
                    if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader["Duties"].ToString() + myReader["WorkerCharacteristics"].ToString() + myReader["MinQualifications"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        In.add(inst);
                    }
                }
                //Console.WriteLine(In.instance(1).stringValue(dataattr));
                myReader.Close();

                SqlCommand myCommand2 = new SqlCommand("select * from ContractorResumes", myConnection);
                SqlDataReader myReader2 = myCommand2.ExecuteReader();

                while (myReader2.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(In);
                    inst.setValue(id, int.Parse(myReader2["Id"].ToString()));
                    inst.setValue(type, 1);
                    if (!(myReader2["ContractorEducation"] == DBNull.Value || /*myReader["ContractorSkills"] == DBNull.Value ||*/ myReader2["ContractorWorkExperience"] == DBNull.Value))
                    //if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader2["ContractorEducation"].ToString() + myReader2["ContractorWorkExperience"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        In.add(inst);
                    }
                }
                myReader2.Close();
                

                weka.filters.unsupervised.attribute.StringToWordVector filter = new weka.filters.unsupervised.attribute.StringToWordVector();
                //filter.setUseStoplist(true);
                String[] options = new String[4];
                options[0] = "-I";
                options[1] = "-S";
                options[2] = "-L";
                options[3] = "-T";
                filter.setOptions(options);
                filter.setStemmer(new weka.core.stemmers.LovinsStemmer());
                weka.core.tokenizers.AlphabeticTokenizer tok = new weka.core.tokenizers.AlphabeticTokenizer();
                
                //tok.setNGramMaxSize(1);
                //tok.setNGramMinSize(1);
                filter.setTokenizer(tok);
                //filter.setOutputWordCounts(true);
                //filter.setIDFTransform(true);
                filter.setInputFormat(In);
                //filter.setWordsToKeep(2000);
                filter.setDoNotOperateOnPerClassBasis(true);
                Instances tfidf = Filter.useFilter(In, filter);
                

                weka.clusterers.SimpleKMeans kmeans = new weka.clusterers.SimpleKMeans();
                kmeans.setNumClusters(21);
                kmeans.setPreserveInstancesOrder(true);
                weka.clusterers.FilteredClusterer fc = new weka.clusterers.FilteredClusterer(); //filtered clusterer to ignore attributes

                String[] options2 = new String[2];
                options2[0] = "-R"; // "range"
                options2[1] = "1,2"; // we want to ignore the attribute that is in the position '1'
                weka.filters.unsupervised.attribute.Remove remove = new weka.filters.unsupervised.attribute.Remove(); // new instance of filter
                remove.setOptions(options); // set options
                remove.setInputFormat(tfidf); // inform filter about dataset
                fc.setFilter(remove); //add filter to remove attributes
                fc.setClusterer(kmeans);
                fc.buildClusterer(tfidf);
                int[] asn = kmeans.getAssignments();

                for (int j = 0; j < tfidf.numInstances(); j++)
                {





                    if (In.instance(j).value(1) == 0)
                    {
                        SqlCommand sqlCmd2 = new SqlCommand("UPDATE dbo.PositionDescriptions SET docCluster=@clusterid WHERE Id=@id", myConnection);
                        sqlCmd2.Parameters.Add("@clusterid", SqlDbType.Int);
                        sqlCmd2.Parameters.Add("@id", SqlDbType.Int);
                        sqlCmd2.Parameters["@clusterid"].Value = asn[j];
                        sqlCmd2.Parameters["@id"].Value = (int)In.instance(j).value(0);
                        sqlCmd2.ExecuteNonQuery();
                    }
                    else
                    {
                        SqlCommand sqlCmd2 = new SqlCommand("UPDATE dbo.ContractorResumes SET docCluster=@clusterid WHERE Id=@id", myConnection);
                        sqlCmd2.Parameters.Add("@clusterid", SqlDbType.Int);
                        sqlCmd2.Parameters.Add("@id", SqlDbType.Int);
                        sqlCmd2.Parameters["@clusterid"].Value = asn[j];
                        sqlCmd2.Parameters["@id"].Value = (int)In.instance(j).value(0);
                        sqlCmd2.ExecuteNonQuery();
                    }
                    
                }



            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
        }
    }
}
