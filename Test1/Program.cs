﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using weka.classifiers;
using weka.classifiers.functions;
using weka.core;
using weka.filters;


namespace Test1
{
    class Program
    {
        private static int useNN = 1;

        static void Main(string[] args)
        {
            //GenerateQueryClusters.gen();
            //TrainNN.train();
            //GenerateDocClusters.genDocClusters();

           //String q = "Learns to catalog, label, file & retrieve magnetic &/or cartridge computer tapes & clean &maintain tapes drives, logs tapes in & out of data library, automated library systems or mixedmedia storage systems, receives training in procedure for scratching/erasing &/or deletingtapes for reuse & operates computer terminals, tape drives & other state-of-art peripheralcomputer equipment (e.g., printers, tape evaluators). Learns to mount tapes on computer tape drives; packages &/or delivers tapes to users orstorage facilities; performs clerical tasks associated with tape storage & use (e.g.,inventories tapes; photocopies materials; completes routine forms); loads & unloads printers. Knowledge of computer tape library system procedures (e.g., filing & retrieval, tape storage& use)*; data processing*; inventory control*; addition, subtraction, multiplication, division,fractions, decimals. Skill in operation of computer terminal, tape drive & peripheral equipment(e.g., printers, tape evaluators). Ability to deal with problems involving few variables withinfamiliar context; complete routine forms; stand, walk or bend continuously; lift up to 50 lbs.of tapes & boxes repeatedly; move fingers repeatedly in use of computer terminal. (*)Developed after employment. Formal education in arithmetic that includes addition, subtraction, multiplication, division,fractions & decimals; 3 mos. trg. or 3 mos. exp. in operation of computer terminal, tape drive& peripheral equipment (e.g., printers, tape evaluators). -Or equivalent of Minimum Class Qualifications For Employment noted above. ";
            //String q = "Assures adequate resources are allocated for each system in proactive manner, determines &maintains memory & processing time requirements for production jobs, monitors productionjob tape usage & print queue volume during peak processing times, follows programming flowcharts, diagrams & instructions to determine job processing placement, most efficient devicetype for each data set & potential data set contention problems & prepares volume table ofcontent (VTOC) reports showing resource availability utilizing job control or report generatinglanguage (e.g., JCL, TSO, Control-M Keystroke Language). Assists higher-level data system schedulers in resolving data set contention problemsbetween production jobs & retention periods for tape data sets & backups; monitors VSAMfiles for signs of inefficient fragmentation; monitors DASD data set space usage to ensurecurrent space allocations are sufficient for production processing & modifies space allocationwhen necessary; builds tables of symbolic parameters used to provide information to systemat execution time. Reviews new & modified operations documentation, updates control-m documentation libraryunder guidance/direction of higher-level data systems scheduler; uses automated schedulingpackages to schedule & monitor production job execution & maintains tables for thesepackages. ";
           //String q = "1. Database support.    a. Advises and consults with users on accessing various databases and to           resolve data conflicts and inappropriate use of data.    b. Issues Tier I resolution.    c. Promotes system security and awareness by adhering to the Agency’s         and/or State’s IT security policy(s) and standards.2. Data modeling.    a. Assists with data model design.    b. Implements physical data models.    c. Assists with tuning a single DBMS system on a single platform.    d. Assists with writing platform specific code.3. Performance tuning and configuration.    a. Provides performance tuning for query language statements.    b. Reads and interprets query plans.    c. Provide monitoring using vendor supplied and 3rd party utilities.4. Database backup/recovery.    a. Restores from disaster recovery plan.    b. Monitors disaster recovery plan.5. Data migrations, business process monitoring.    a. Designs and implements processes for the efficient transfer of data.    b. Monitors data transfer processes.    c. Resolves data conflicts and inappropriate use of data.    d. Provides ETL and data warehouse support.    e. Transfers data for data warehouse, build tables and load scripts.    f. Writes platform specific code for data migrations.6. Database security.    a. Sets security and validates all updates through database connections.    b. Collaborates with system administrator to set table access security.7. High availability.    a. Monitors cluster parameters for high performance.    b. Resolves problems and issues.8. Installation, patching, versioning patching configuration.     a. Performs database setup and configuration to support application        development projects (both purchased and custom) including database         software installation, database creation, schema setup, file layout, and         migration scripting using established methodology and process.9. Mentoring.     a. Receives orientation related to mentorship and applies as necessary (e.g.,          definition, purpose, strategies, and evaluation techniques Knowledge of: oral and written communication tools and techniques, customer support andpersonal service, technical writing and documentation practices, technology design,mathematic principles relative to assigned area in IT, IT principles, methods and practices inassigned specialty area, state and agency policy, procedures and applicable laws*, vision,mission and goals of agency*.  Skill for: reading comprehension, speaking, service orientation, troubleshooting, criticalthinking, and using data recovery tools and techniques. Ability to: prepare meaningful,accurate and concise reports, and stay abreast of current technologies in area of ITassigned.(*)Developed after employment. Education    Core undergraduate program in Computer Science or Information Systems orequivalent work experience.AND  Experience Minimum 2 years combined work experience in any combination of the following:database administration providing Tier I support to information system users and/or ITstaff, assisting with data modeling and conducting performance tuning andconfiguration. Note: the official position description on file with the designated agency is to reflect, in the minimumacceptable characteristics, the required technical experience. Only those applicants possessing therequired technical experience listed in the position description are to be considered for any vacanciesposted. The vacancy/job posting should also only list the required technical experience commensurate withthe position in question.";
           String q = "Data Entry Operator 1Class Code:12331 Pay Range: 4STA TE O F O HIO  Revis ion Date: May 1 , 2008SALARY RANGE$14.36 - $15.41 Hourly$2,489.07 - $2,671.07 Monthly$29,868.80 - $32,052.80 AnnuallyBargaining Unit:  C09 - AFSCME Major Agencies: All Agencies SERIES PURPOSE:. The purpose of the data entry series is the input, verification & editing of raw data intomachine-readable form. At the lower-levels, incumbents do data entry &/or verification. Atthe middle level, incumbents act as lead worker. At the higher levels, incumbents supervisedata entry operators &/or supervisors. At the highest level, incumbents manage the overalloperations of multiple-location data entry operations. CLASS CONCEPT:The entry level class works under immediate supervision & requires some knowledge of unitpolicies & procedures regarding basic data entry in order to input machine-readable data. JOB DUTIES IN ORDER OF IMPORTANCE:Operates keyboard on data entry equipment (e.g., key to disk, tape or card, video displayterminal), operates electric typewriter keyboard to transfer coded data to free or preprintedforms for optical scanner. Performs variety of clerical tasks related to data processing work (e.g., proofreads, files,distributes & mails output, totals data, maintains entry logs). Knowledge of addition, subtraction, multiplication & division. Skill in typing; operation of dataentry equipment*, operation of peripheral machines (e.g., scanner, printer)*. Ability to checkpairs of items that are similar or dissimilar; read, copy & record figures; proofread material,recognize errors & make corrections; move limbs, fingers easily to perform manual functionsrepeatedly. (*)Developed after employment. Formal education in arithmetic that includes addition, subtraction, multiplication & division &in writing & speaking common English vocabulary; 1 course or 3 mos. exp. in typing. -Or equivalent of Minimum Class Qualifications For Employment noted above. ";
            int queryCluster = ClusterQuery.clusterQuery(q);
            Console.WriteLine("cluster query returned " + queryCluster);
           
            SqlConnection myConnection = new SqlConnection("user id=shivhare.1;" +
                                       "password=abc@123;server=localhost;" +
                                       "Trusted_Connection=yes;" +
                                       "database=test; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select docCluster from PositionDescriptions where queryCluster=" + queryCluster, myConnection);
                myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    int docCluster = int.Parse(myReader["docCluster"].ToString());
                    Console.WriteLine("doc cluster query returned " + docCluster);
                    myReader.Close();
                    if (useNN == 1)
                    {
                        int ret = ClassifyQuery.classify(q, docCluster);
                        if (ret == 0)
                        {
                            SqlCommand myCommand2 = new SqlCommand("select Id from ContractorResumes where docCluster=" + docCluster, myConnection);
                            myReader = myCommand2.ExecuteReader();
                            while (myReader.Read())
                                Console.WriteLine("doc in cluster id= " + int.Parse(myReader["Id"].ToString()));
                        }
                    }
                    else
                    {
                        SqlCommand myCommand2 = new SqlCommand("select Id from ContractorResumes where docCluster=" + docCluster, myConnection);
                        myReader = myCommand2.ExecuteReader();
                        while (myReader.Read())
                            Console.WriteLine("doc in cluster id= " + int.Parse(myReader["Id"].ToString()));
                    }
                    
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadKey();
        }
    }
}