﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using weka.classifiers;
using weka.classifiers.functions;
using weka.core;
using weka.filters;

namespace Test1
{
    class ClassifyQuery
    {
        public static int classify(String q, int NNid)
        {
            int numres = 0;
            SqlConnection myConnection = new SqlConnection("user id=shivhare.1;" +
                                       "password=abc@123;server=localhost;" +
                                       "Trusted_Connection=yes;" +
                                       "database=test; " +
                                       "connection timeout=30");
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {
                List<String> docs = new List<String>();
                FastVector attrs = new FastVector();
                weka.core.Attribute id = new weka.core.Attribute("id");
                attrs.addElement(id);
                weka.core.Attribute type = new weka.core.Attribute("type");
                attrs.addElement(type);
                weka.core.Attribute pref = new weka.core.Attribute("pref");
                attrs.addElement(pref);
                weka.core.Attribute dataattr = new weka.core.Attribute("data", (FastVector)null);
                attrs.addElement(dataattr);

                Instances resumes = new Instances("resumes", attrs, 10000);
                SqlCommand myCommand2 = new SqlCommand("select * from ContractorResumes", myConnection);
                SqlDataReader myReader2 = myCommand2.ExecuteReader();

                while (myReader2.Read())
                {
                    Instance inst = new Instance(attrs.size());
                    inst.setDataset(resumes);
                    inst.setValue(id, int.Parse(myReader2["Id"].ToString()));
                    inst.setValue(type, 1);
                    if (!(myReader2["prefCluster"] == DBNull.Value))
                        inst.setValue(pref, int.Parse(myReader2["prefCluster"].ToString()));
                    else
                        inst.setValue(pref, 0);
                    if (!(myReader2["ContractorEducation"] == DBNull.Value || /*myReader["ContractorSkills"] == DBNull.Value ||*/ myReader2["ContractorWorkExperience"] == DBNull.Value))
                    //if (!(myReader["Duties"] == DBNull.Value || myReader["WorkerCharacteristics"] == DBNull.Value || myReader["MinQualifications"] == DBNull.Value))
                    {
                        String data = /*myReader["ContractorCertifications"].ToString() +*/ myReader2["ContractorEducation"].ToString() + myReader2["ContractorWorkExperience"].ToString();
                        //docs.Add(data);
                        inst.setValue(dataattr, data);
                        resumes.add(inst);
                    }
                }
                myReader2.Close();
                FastVector attrs2 = new FastVector();
                weka.core.Attribute classdata = new weka.core.Attribute("class");
                attrs2.addElement(classdata);
                weka.core.Attribute resumedata = new weka.core.Attribute("resumedata", (FastVector)null);
                attrs2.addElement(resumedata);
                weka.core.Attribute resumedata2 = new weka.core.Attribute("resumedata2", (FastVector)null);
                attrs2.addElement(resumedata2);
                Instances temp = new Instances("dataset", attrs2, 1000000);
                for (int i = 0; i < resumes.numInstances(); i++)
                {

                    Instance inst = new Instance(attrs2.size());
                    inst.setDataset(temp);
                    inst.setValue(resumedata, resumes.instance(i).stringValue(3));
                    inst.setValue(resumedata2, q);
                    temp.add(inst);
                }
                weka.filters.unsupervised.attribute.StringToWordVector filter = new weka.filters.unsupervised.attribute.StringToWordVector();
                //filter.setUseStoplist(true);
                String[] options = new String[4];
                options[0] = "-I";
                options[1] = "-S";
                options[2] = "-L";
                options[3] = "-T";
                filter.setOptions(options);
                filter.setStemmer(new weka.core.stemmers.LovinsStemmer());
                weka.core.tokenizers.AlphabeticTokenizer tok = new weka.core.tokenizers.AlphabeticTokenizer();

                //tok.setNGramMaxSize(1);
                //tok.setNGramMinSize(1);
                filter.setTokenizer(tok);
                //filter.setOutputWordCounts(true);
                //filter.setIDFTransform(true);
                filter.setInputFormat(temp);
                filter.setWordsToKeep(11);
                filter.setDoNotOperateOnPerClassBasis(true);
                Instances tfidf = Filter.useFilter(temp, filter);
                if (tfidf.numAttributes() != TrainNN.NNnumattrs)
                {
                    while (tfidf.numAttributes() != TrainNN.NNnumattrs)
                        tfidf.deleteAttributeAt(tfidf.numAttributes() - 1);
                }
                SqlCommand myCommand3 = new SqlCommand("select * from models", myConnection);
                SqlDataReader myReader3 = myCommand3.ExecuteReader();
                MultilayerPerceptron NN = null;
                while (myReader3.Read())
                {
                    if (int.Parse(myReader3["id"].ToString()) == NNid)
                    {
                        MemoryStream memStream = new MemoryStream((byte[])myReader3["Data"]);
                        BinaryFormatter binForm = new BinaryFormatter();
                        memStream.Seek(0, SeekOrigin.Begin);
                        NN = (MultilayerPerceptron)binForm.Deserialize(memStream);
                    }
                }
                for (int i = 0; i < tfidf.numInstances(); i++)
                {
                    if (NN == null)
                        break;
                    else
                    {
                        if (NN.classifyInstance(tfidf.instance(i)) > .3)
                        {
                            numres++;
                            Console.WriteLine(resumes.instance(i).value(0));
                        }
                    }
                }
                Console.WriteLine("number of results " + numres);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return numres;

        }
    }
}
